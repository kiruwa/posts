from django.db import models
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):
    name = models.CharField(max_length=200)


class Repost(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE, related_name='repost_author', null=True, unique=False)
    post = models.ForeignKey('posts.Post', on_delete=models.CASCADE, related_name='post', null=True, unique=False)
    repost_created_date = models.DateTimeField(auto_now_add=True)
    sign = models.TextField()

    class Meta:
        unique_together = (('user', 'post'),)


class Post(models.Model):
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    text = models.TextField()
    created_date = models.DateTimeField(auto_now_add=True)
    likes = models.ManyToManyField(CustomUser, related_name='likes', blank=True)
    repost = models.ManyToManyField(CustomUser, related_name='repost',
                                    through=Repost)

    def __str__(self):
        return self.text

    @property
    def total_likes(self):
        return self.likes.all().count()