from django.contrib.auth import get_user_model

from .models import Post

User = get_user_model()


def add_like(post: Post, user: User):
    """Лайкае `obj`.
    """
    like = post.likes.add(user)
    return like


def remove_like(post: Post, user: User):
    """Удаляет лайк с `obj`.
    """
    post.likes.remove(user)


def is_fan(post: Post, user: User) -> bool:
    """Проверяет, лайкнул ли `user` `obj`.
    """
    if not user.is_authenticated:
        return False
    post.likes.filter(username=user.username)
    return post.likes.exists()


def get_fans(likes):
    """Получает всех пользователей, которые лайкнули `obj`.
    """
    return User.objects.filter(likes=likes)
