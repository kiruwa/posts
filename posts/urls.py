from django.urls import path, include
from rest_framework.routers import DefaultRouter
from . import views
from .views import login, PostsList, CreateUserView, CustomAuthToken, Logout, PostViewSet

router = DefaultRouter()
router.register(r'posts', views.PostViewSet)
router.register(r'users', views.UserViewSet)
urlpatterns = [
    path('', include(router.urls)),
    path('api/login/', login),
    path('users/<str:author>/posts/', PostsList.as_view()),
    path('register/', CreateUserView.as_view()),
    path('api-token-auth/', CustomAuthToken.as_view()),
    path('logout/', Logout.as_view()),
    path('posts/<int:pk>/repost/', PostViewSet.repost),
]
