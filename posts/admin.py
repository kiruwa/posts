from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser, Post


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ['email', 'username', 'name', ]


class CustomPostAdmin(admin.ModelAdmin):
    model = Post
    list_display = ['author', 'created_date', 'total_likes']


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Post, CustomPostAdmin)
