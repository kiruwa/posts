from rest_framework import serializers

from .models import CustomUser, Post, Repost
from . import services


class UserSerializer(serializers.HyperlinkedModelSerializer):

    class Meta:
        model = CustomUser
        fields = ('url', 'id', 'username', 'name', 'email',)

    def perform_create(self, validated_data):
        user = CustomUser.objects.create(validated_data['username'], validated_data['email'],
                                         validated_data['name'], validated_data['password'])

        user.set_password(validated_data['password'])
        user.save()
        return user


class PostSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')
    is_fan = serializers.SerializerMethodField()
    tags = serializers.ReadOnlyField(source='tag.text')

    class Meta:
        model = Post
        fields = ('url', 'id', 'author', 'text', 'total_likes', 'is_fan', 'tags')

    def get_is_fan(self, obj) -> bool:
        """Проверяет, лайкнул ли `request.user` твит (`obj`).
        """
        user = self.context.get('request').user
        return services.is_fan(obj, user)


class RepostSerializer(serializers.ModelSerializer):
    sign = serializers.CharField(max_length=200)
    repost_created_date = serializers.ReadOnlyField()

    class Meta:
        model = Repost
        fields = ('post', 'repost_created_date', 'user', 'sign', 'id')
