from rest_framework.decorators import action
from rest_framework.response import Response

from . import services
from .serializers import UserSerializer


class LikedMixin:

    @action(methods=['POST'], detail=True)
    def like(self, request, pk=None):
        """Лайкает `obj`.
        """
        post = self.get_object()
        services.add_like(post, request.user)
        return Response()

    @action(methods=['POST'], detail=True)
    def unlike(self, request, pk=None):
        """Удаляет лайк с `obj`.
        """
        obj = self.get_object()
        services.remove_like(obj, request.user)
        return Response()

    @action(methods=['GET'], detail=True)
    def fans(self, request, pk=None):
        """Получает всех пользователей, которые лайкнули `obj`.
        """
        obj = self.get_object()
        fans = services.get_fans(obj)
        serializer_context = {
            'request': request,
        }
        serializer = UserSerializer(fans, context=serializer_context, many=True)
        return Response(serializer.data)
